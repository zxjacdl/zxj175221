import java.util.*;
public class MyList {
    public static void main(String [] args) {
        //选用合适的构造方法，用你学号前后各两名同学的学号创建四个结点
        List<String> list=new LinkedList<String>();
        list.add("20175219");
        list.add("20175220");
        list.add("20175222");
        list.add("20175223");
        System.out.println("初始链表：");
        //把上面四个节点连成一个没有头结点的单链表
        Iterator<String> iter=list.iterator();
        while(iter.hasNext()){
            String danlianbiao=iter.next();
            System.out.println(danlianbiao);
        }
        //遍历单链表，打印每个结点的
        list.add("20175221");
        //把你自己插入到合适的位置（学号升序）
        System.out.println("插入学号后排序，打印链表:");
        Collections.sort(list);
        iter=list.iterator();
        while(iter.hasNext()){
            String danlianbiao=iter.next();
            System.out.println(danlianbiao);
        }
        //遍历单链表，打印每个结点的
        list.remove("20175221");
        //从链表中删除自己
        System.out.println("删除学号后,打印链表:");
        iter=list.iterator();
        while(iter.hasNext()){
            String danlianbiao=iter.next();
            System.out.println(danlianbiao);
        }
        //遍历单链表，打印每个结点的
    }
}
