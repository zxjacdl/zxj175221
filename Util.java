import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Util {
    static String Filename;
    public static String main() {
        List<String> FilePath = new ArrayList<>();
        System.out.println("请输入文件名:");
        Scanner scan = new Scanner(System.in);
        Filename = scan.nextLine();
        System.out.println("A.当前盘查找\n" + "B.指定文件夹查找\n");
        Scanner pan = new Scanner(System.in);
        String str = pan.nextLine();
        switch (str) {
            case "A":
                List<File> files = searchFiles(new File("G:"), Filename);
                System.out.println("共找到:" + files.size() + "个文件");
                for (File file : files) {
                    FilePath.add(file.getAbsolutePath());
                    return file.getAbsolutePath();
                }
                break;
            case "B":
                System.out.println("请输入需要查找的位置:");
                Scanner canf = new Scanner(System.in);
                String pathname = canf.nextLine();
                List<File> files1 = searchFiles(new File(pathname), Filename);
                System.out.println("共找到:" + files1.size() + "个文件");
                for (File file : files1) {
                    FilePath.add(file.getAbsolutePath());
                    System.out.println(file.getAbsolutePath());
                }
                System.out.println("\n");
                System.out.println(FilePath.get(1));
                break;
                default:
        }
        return str;
    }
    public static List<File> searchFiles(File folder, final String keyword) {
        List<File> result = new ArrayList<File>();
        if (folder.isFile()) {
            result.add(folder);
        }

        File[] subFolders = folder.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                if (file.isDirectory()) {
                    return true;
                }
                return file.getName().toLowerCase().contains(keyword);
            }
        });

        if (subFolders != null) {
            for (File file : subFolders) {
                if (file.isFile()) {
                    // 如果是文件则将文件添加到结果列表中
                    result.add(file);
                } else {
                    // 如果是文件夹，则递归调用本方法，然后把所有的文件加到结果列表中
                    result.addAll(searchFiles(file, keyword));
                }
            }
        }
        return result;
    }
}

