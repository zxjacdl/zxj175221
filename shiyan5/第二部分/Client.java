import java.io.*;
import java.net.*;

public class Client {
    public static void main(String args[]) {
        System.out.println("20175221 正在启动...");//向服务器端发送信息
        Socket mysocket;//创建客户端Socket
        DataInputStream in = null;
        DataOutputStream out = null;
        try {
            mysocket = new Socket("169.254.245.151", 2010);//客户端指向服务器地址和端口
            in = new DataInputStream(mysocket.getInputStream());
            out = new DataOutputStream(mysocket.getOutputStream());
            System.out.println("请输入中缀表达式:");//向本机的2010端口发出客户请求
            String string = new BufferedReader(new InputStreamReader(System.in)).readLine();
            MyBC conv = new MyBC();
            conv.conversion(string);
            String string1 = conv.getInformation();
            out.writeUTF(string1);
            String reply = in.readUTF();//in读取信息，堵塞状态
            System.out.println("20175221 收到 20175120 的回答:\n" + reply);// 从Server读入字符串，并打印
            Thread.sleep(2000);
        } catch (Exception e) {
            System.out.println("20175120 已掉线" + e);//输出异常
        }
    }
}

