import java.io.*;
import java.net.*;

public class Server {
    public static void main(String[] args) throws IOException {
        int reply;
        ServerSocket serverForClient = null;
        Socket socketOnServer = null;
        DataOutputStream out = null;
        DataInputStream in = null;
        try {
            serverForClient = new ServerSocket(2010);
        } catch (IOException e1) {
            System.out.println(e1);
        }
        try {
            System.out.println("等待 20175221 呼叫...");
            socketOnServer = serverForClient.accept(); //堵塞状态，除非20175221呼叫
            in = new DataInputStream(socketOnServer.getInputStream());
            out = new DataOutputStream(socketOnServer.getOutputStream());
            String information = in.readUTF(); // in读取信息，堵塞状态
            System.out.println("20175120 收到 20175221 的提问:" + information);
            MyDC mydc = new MyDC();
            reply = mydc.evaluate(information);
            out.writeUTF(reply + "");
            Thread.sleep(2000);
        } catch (Exception e) {
            System.out.println("20175221 已掉线" + e);
        }
    }
}

