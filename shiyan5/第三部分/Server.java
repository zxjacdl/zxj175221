import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String args[]) {
        MyDC mydc = new MyDC();
        ServerSocket serverForClient = null;
        Socket socketOnServer = null;
        DataOutputStream out = null;
        DataInputStream in = null;
        try {
            serverForClient = new ServerSocket(5221);
        } catch (IOException e1) {
            System.out.println(e1);
        }
        try {
            System.out.println("等待 20175221 下达命令...");
            socketOnServer = serverForClient.accept(); //堵塞状态，除非 20175221 呼叫
            System.out.println("20175221 终于连接上了");
            out = new DataOutputStream(socketOnServer.getOutputStream());
            in = new DataInputStream(socketOnServer.getInputStream());
            String str = in.readUTF(); // in读取信息，堵塞状态
            byte ctext[] = new byte[Integer.parseInt(str)];
            for (int i = 0;i<Integer.parseInt(str);i++) {
                String temp = in.readUTF();
                ctext[i] = Byte.parseByte(temp);
            }
            // 获取密钥
            FileInputStream f = new FileInputStream("keykb1.dat");
            int num = f.available();
            byte[] keykb = new byte[num];
            f.read(keykb);
            SecretKeySpec k = new SecretKeySpec(keykb, "DESede");
            // 解密
            Cipher cp = Cipher.getInstance("DESede");
            cp.init(Cipher.DECRYPT_MODE, k);
            byte[] ptext = cp.doFinal(ctext);
            System.out.println("");
            // 显示明文
            String p = new String(ptext,"UTF8");
            String s = mydc.evaluate(p)+"";
            System.out.println("解密后的后缀表达式：\n" + p);
            System.out.println("开始计算后缀表达式:\n" + p);
            System.out.println("后缀表达式的值为:\n" + s);
            out.writeUTF(mydc.evaluate(p)+"");
            System.out.println("20175120 正在将值传给 20175221...");
        } catch (Exception e) {
            System.out.println("20175221 已掉线..." + e);
        }
    }
}

