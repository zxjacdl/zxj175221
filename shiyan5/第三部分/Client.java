import javax.crypto.Cipher;
import java.io.*;
import java.net.Socket;
import java.security.Key;
import java.util.Scanner;

public class Client {
    public static void main(String args[]) {
        Socket mysocket;
        MyBC mybc = new MyBC();
        DataInputStream in = null;
        DataOutputStream out = null;
        Scanner scanner = new Scanner(System.in);
        String string;
        try {
            mysocket = new Socket("169.254.245.151", 5221);
            in = new DataInputStream(mysocket.getInputStream());
            out = new DataOutputStream(mysocket.getOutputStream());
            System.out.println("20175221 正在启动...");
            FileInputStream f = new FileInputStream("key1.dat");
            ObjectInputStream b = new ObjectInputStream(f);
            Key key = (Key) b.readObject();
            Cipher cp = Cipher.getInstance("DESede");
            cp.init(Cipher.ENCRYPT_MODE, key);
            System.out.println("请输入中缀表达式：");
            string = scanner.nextLine();
            mybc.conversion(string);
            String string1 = mybc.getInformation();
            byte ptext[] = string1.getBytes("UTF-8");
            byte ctext[] = cp.doFinal(ptext);
            System.out.println("加密后的后缀表达式：");
            for (int i = 0; i < ctext.length; i++) {
                System.out.print(ctext[i] + ",");
            }
            System.out.println("");
            out.writeUTF(ctext.length + "");
            for (int i = 0; i < ctext.length; i++) {
                out.writeUTF(ctext[i] + "");
            }
            String s = in.readUTF();
            System.out.println("20175221 收到 20175120 的回应:\n" + s);
        } catch (Exception e) {
            System.out.println("20175120 已掉线..." + e);
        }
    }
}

