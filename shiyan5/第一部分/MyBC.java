import java.util.*;
/**
 * @author 20175221 Zxj
 */
public class MyBC {
    private Stack<String> stack;
    private List<String> list;
    private String information;
    private String Information = "";
    public MyBC() {
        stack = new Stack<String>();//设立一个栈，存放运算符
        list = new ArrayList<String>();//创建一个表，存放操作数及运算符
    }
    public void conversion(String exp) {   //中缀转后缀
        String element ;
        StringTokenizer tokenizer = new StringTokenizer(exp);
        while (tokenizer.hasMoreTokens()) {  //当tokenizer有下一个值时，进行循环，并把值赋给element
            element  = tokenizer.nextToken();
            if (element.equals("(")) {  //若是左括号，入栈
                stack.push(element);
            }
            else if (element.equals("+") || element.equals("-")) {  //若是“+”或“-”，继续判断栈是否为空
                if (!stack.empty()) {  //若栈非空，判断栈顶元素
                    if (stack.peek().equals("(")) {  //若栈顶为“（”，运算符入栈
                        stack.push(element);
                    }
                    else {  //否则先把栈顶元素移除，加到表中，再将运算符入栈
                        list.add(stack.pop());
                        stack.push(element);
                    }
                }
                else {  //若栈为空，运算符入栈
                    stack.push(element);
                }
            }
            else if (element.equals("*") || element.equals("/")) {  //若是“*”或“/”，继续判断栈是否为空
                if (!stack.empty()) {  //若栈非空，判断栈顶元素是什么
                    if (stack.peek().equals("*") || stack.peek().equals("/")) {  //若栈顶为“*”或“/”，先把栈顶元素移除，加到表中，再将运算符入栈
                        list.add(stack.pop());
                        stack.push(element);
                    }
                    else {  //若栈顶为其他，运算符直接入栈
                        stack.push(element);
                    }
                }
                else {  //若栈为空，运算符直接入栈
                    stack.push(element);
                }
            }
            else if (element.equals(")")) {  //若遇到“）”，开始循环
                while (true) {  //先把栈顶元素移除并赋给temp
                    String temp = stack.pop();
                    if (!temp.equals("(")) {  //若temp不为“（”，则加到表
                        list.add(temp);
                    }
                    else {  //若temp为“（”，退出循环
                        break;
                    }
                }
            }
            else {  //若为操作数，进入列表
                list.add(element);
            }
        }
        while (!stack.empty()) {  //将栈中元素取出，加到列表中，直到栈为空
            list.add(stack.pop());
        }
        ListIterator<String> List = list.listIterator();  //返回此列表元素的列表迭代器
        while (List.hasNext()) {  //将迭代器中的元素依次取出，并加上空格作为分隔符
            Information += List.next() + " ";
            List.remove();
        }
        information = Information;
    }

    public String getInformation() {
        return information;
    }
}

