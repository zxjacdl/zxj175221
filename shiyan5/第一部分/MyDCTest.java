import java.util.*;
public class MyDCTester {
    public static void main(String[] args) {
        int result;
        String exp;
        MyBC bc = new MyBC();
        MyDC dc = new MyDC();
        System.out.println("请输入中缀表达式:");
        Scanner in = new Scanner(System.in);
        exp = in.nextLine();
        bc.conversion(exp);
        System.out.println("后缀表达式为:\n"+bc.getInformation());
        result = dc.evaluate(bc.getInformation());
        System.out.println("计算结果为:\n"+result);
    }
}

