import java.util.*;

public class MyDC {
    private final char ADD = '+';
    private final char SUBTRACT = '-';
    private final char MUTIPLY = '*';
    private final char DIVIDE = '/';
    private Stack<Integer> stack;
    public MyDC(){
        stack = new Stack<Integer>();
    }
    public int evaluate(String exp){
        int op1,op2,result = 0;
        String element;
        StringTokenizer tokenizer = new StringTokenizer(exp);
        while(tokenizer.hasMoreTokens()){
            element = tokenizer.nextToken();
            if(isOperator(element)){
                op2 = (stack.pop().intValue());
                op1 = (stack.pop().intValue());
                result = evalSingleOp(element.charAt(0),op1,op2);
                stack.push(new Integer(result));
            }
            else {
                stack.push(new Integer((Integer.parseInt(element))));
            }
        }
        return result;
    }
    private boolean isOperator(String element){
        return (element.equals("+")||element.equals("-")||element.equals("*")||element.equals("/"));
    }
    private int evalSingleOp(char operation,int op1,int op2) {
        int result = 0;
        switch (operation){
            case ADD:
                result = op1+op2;
                break;
            case SUBTRACT:
                result = op1-op2;
                break;
            case MUTIPLY:
                result = op1*op2;
                break;
            case DIVIDE:
                result = op1/op2;
        }
        return result;
    }
}

