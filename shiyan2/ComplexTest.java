import junit.framework.TestCase;
import org.junit.Test;

public class ComplexTest extends TestCase {
    Complex a = new Complex(0.0, 2.0);
    Complex b = new Complex(-1.0, -1.0);
    Complex c = new Complex(1.0,2.0);
    @Test
    public void testgetRealPart() throws Exception {
        assertEquals(0.0, Complex.getRealPart(0.0));
        assertEquals(-1.0, Complex.getRealPart(-1.0));
        assertEquals(1.0, Complex.getRealPart(1.0));
    }
    @Test
    public void testgetImagePart() throws Exception {
        assertEquals(2.0, Complex.getImagePart(2.0));
        assertEquals(-1.0, Complex.getImagePart(-1.0));
        assertEquals(2.0, Complex.getImagePart(2.0));
    }
    @Test
    public void testComplexAdd() throws Exception {
        assertEquals("-1.0+1.0i", a.ComplexAdd(b).toString());
        assertEquals("1.0+4.0i", a.ComplexAdd(c).toString());
        assertEquals("0.0+1.0i", b.ComplexAdd(c).toString());
    }
    @Test
    public void testComplexSub() throws Exception {
        assertEquals("1.0+3.0i", a.ComplexSub(b).toString());
        assertEquals("-1.0", a.ComplexSub(c).toString());
        assertEquals("-2.0 -3.0i", b.ComplexSub(c).toString());
    }
    @Test
    public void testComplexMulti() throws Exception {
        assertEquals("2.0 -2.0i", a.ComplexMulti(b).toString());
        assertEquals("-4.0+2.0i", a.ComplexMulti(c).toString());
        assertEquals("1.0 -3.0i", b.ComplexMulti(c).toString());
    }
    @Test
    public void testComplexComplexDiv() throws Exception {
        assertEquals("-1.0 -1.0i", a.ComplexDiv(b).toString());
        assertEquals("0.4+0.8i", a.ComplexDiv(c).toString());
        assertEquals("-0.6 -0.6i", b.ComplexDiv(c).toString());
    }
}
