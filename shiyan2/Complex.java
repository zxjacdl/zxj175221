public class Complex{
    private double real;
    private double image;

    public Complex(double real, double image) {
        this.real = real;
        this.image = image;
    }

    public static double getRealPart(double real) {
        return real;
    }

    public static double getImagePart(double image) {
        return image;
    }

    public Complex ComplexAdd(Complex c) {
        return new Complex(real + c.real, image + c.image);
    }
    public Complex ComplexSub(Complex c) {
        return new Complex(real - c.real, image - c.image);
    }
    public Complex ComplexMulti(Complex c) {
        return new Complex(real * c.real - image * c.image, real * c.image + image * c.real);
    }
    public Complex ComplexDiv(Complex c) {
        return new Complex((real * c.image + image * c.real)/(c.image * c.image + c.real * c.real), (image * c.image + real * c.real)/(c.image * c.image + c.real * c.real));
    }

    public String toString() {
        String s = " ";
        if (image > 0)
            s =  real + "+" + image + "i";
        if (image == 0)
            s =  real + "";
        if (image < 0)
            s = real + " " + image + "i";
        return s;
    }
}
