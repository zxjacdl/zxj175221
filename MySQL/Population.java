import java.sql.*;

public class Population {
    public static void main(String[] args) {
        Connection con;
        Statement sql;
        ResultSet rs;
        con = GetDBConnection.connectDB("world","root","zxja31415926");
        if(con == null) {
            return;
        }
        String sqlStr = "select * from country where Region = 'Middle East'";
        try {
            sql = con.createStatement();
            rs = sql.executeQuery(sqlStr);
            long totalpopulation = 0;
            while(rs.next()) {
                int Population = rs.getInt(7);
                totalpopulation +=Population;
            }
            System.out.println("中东国家的总人口为:"+totalpopulation);
            con.close();
        }
        catch (SQLException e) {
            System.out.println(e);
        }
    }
}


