import java.sql.*;

public class City {
    public static void main(String[] args) {
        Connection con;
        Statement sql;
        ResultSet rs;
        con = GetDBConnection.connectDB("world","root","zxja31415926");
        if(con == null) {
            return;
        }
        try {
            sql=con.createStatement();
            rs = sql.executeQuery("SELECT * FROM city where Population>3017522");
            while (rs.next()) {
                int ID = rs.getInt(1);
                String Name = rs.getString(2);
                String CountryCode = rs.getString(3);
                String District = rs.getString(4);
                int Population =rs.getInt(5);
                if(Population>3017522) {
                    System.out.printf("%d\t", ID);
                    System.out.printf("%s\t", Name);
                    System.out.printf("%s\t", CountryCode);
                    System.out.printf("%s\t", District);
                    System.out.printf("%d\n", Population);
                }
            }
            con.close();
        }
        catch (SQLException e) {
            System.out.println(e);
        }
    }
}
