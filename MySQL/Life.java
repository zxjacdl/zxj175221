import java.sql.*;

public class Life {
    public static void main(String[] args) {
        Connection con;
        Statement sql;
        ResultSet rs;
        con = GetDBConnection.connectDB("world","root","zxja31415926");
        if(con == null) {
            return;
        }
        String sqlStr = "select * from country order by LifeExpectancy";
        try {
            sql = con.createStatement();
            rs = sql.executeQuery(sqlStr);
            rs.first();
            String highcountry,lowcountry;
            float number1 = rs.getInt(8);
            while(number1 == 0) {
                rs.next();
                number1 = rs.getInt(8);
            }
            lowcountry = rs.getString(2);
            System.out.println("世界上平均寿命最短的国家为："+lowcountry+" 寿命为"+number1);
            rs.last();
            float number2 = rs.getInt(8);
            highcountry = rs.getString(2);
            System.out.println("世界上平均寿命最长的国家为："+highcountry+" 寿命为"+number2);
            con.close();
        }
        catch (SQLException e) {
            System.out.println(e);
        }
    }
}


