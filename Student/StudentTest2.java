import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

class StudentTest2 {
    public static void main(String[] args) {
        List<Student2> lianbiao = new LinkedList<Student2>();//创建一个链表

        //向链表中添加我与前后各两名同学
        lianbiao.add(new Student2("20175221", "曾祥杰",100,90,100));
        lianbiao.add(new Student2("20175220", "张斌",86,93,96));
        lianbiao.add(new Student2("20175219", "罗乐琦",95,93,97));
        lianbiao.add(new Student2("20175223", "姚明宇",88,90,95));
        lianbiao.add(new Student2("20175222", "罗雨石",79,80,93));

        Iterator<Student2> iter = lianbiao.iterator();//迭代
        //输出一开始的排序:
        System.out.println("一开始的排序:");
        while (iter.hasNext()) {
            Student2 student2 = iter.next();
            System.out.println("姓名："+student2.getName() +" ID："+student2.getId() +  " 总成绩：" + student2.getTotalScore());
        }
        //调用接口中的Compare1方法排序Id打印链表
        Collections.sort(lianbiao,new Compare1());
        System.out.println("按学号升序排序:");
        iter = lianbiao.iterator();
        while (iter.hasNext()){
            Student2 student2 = iter.next();
            System.out.println("姓名："+student2.getName() + " ID："+student2.getId() + " " +  " 总成绩：" + student2.getTotalScore());
        }
        //调用接口中的Compare2方法排序总分后打印链表
        Collections.sort(lianbiao,new Compare2());
        System.out.println("按总成绩升序排序:");
        iter = lianbiao.iterator();
        while (iter.hasNext()){
            Student2 student2 = iter.next();
            System.out.println("姓名："+student2.getName() + " ID："+student2.getId() + " 总成绩：" + student2.getTotalScore());
        }
    }
}



