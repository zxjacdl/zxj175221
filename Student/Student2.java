import java.util.Comparator;

//Compare方法1
class Compare1 implements Comparator {
    @Override
    public int compare(Object b1, Object b2) {
        Student2 st1 = (Student2) b1;
        Student2 st2 = (Student2) b2;
        int Id1 = Integer.parseInt(((Student2) b1).getId());
        int Id2 = Integer.parseInt(((Student2) b2).getId());
        return (Id1 - Id2);
    }
}
//Compare方法2
class Compare2 implements Comparator {
    @Override
    public int compare(Object b1, Object b2) {
        Student2 student1 = (Student2) b1;
        Student2 student2 = (Student2) b2;
        if (student1.getTotalScore() > student2.getTotalScore()) {
            return 1;
        }
        if (student1.getTotalScore() == student2.getTotalScore()) {
            return 0;
        }
        if (student1.getTotalScore() <= student2.getTotalScore()) {
            return -1;
        }
        return 0;
    }
}
class Student2 {
    private char sex;
    private String id;//表示学号
    private String name;//表示姓名
    private int age;//表示年龄
    private double computer_score;//表示计算机课程的成绩
    private double english_score;//表示英语课的成绩
    private double maths_score;//表示数学课的成绩
    private double total_score;// 表示总成绩
    private double ave_score; //表示平均成绩

    public Student2(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Student2(String id, String name, char sex, int age) {
        this(id, name);
        this.sex = sex;
        this.age = age;
    }

    public Student2(String id, String name, double computer_score, double english_score, double maths_score) {
        this(id, name);
        this.computer_score = computer_score;
        this.english_score = english_score;
        this.maths_score = maths_score;
    }

    public String getId() {
        return id;
    }//获得当前对象的学号
    public String getName() {
        return name;
    }//获得当前对象的姓名
    public double getComputer_score() {
        return computer_score;
    }//获得当前对象的计算机课程成绩,
    public double getMaths_score() {
        return maths_score;
    }//获得当前对象的数学课程成绩,
    public double getEnglish_score() {
        return english_score;
    }//获得当前对象的英语课程成绩,

    public void setId(String id) {
        this.id = id;
    }// 设置当前对象的id值,
    public void setComputer_score(double computer_score) {
        this.computer_score = computer_score;
    }//设置当前对象的Computer_score值,
    public void setEnglish_score(double english_score) {
        this.english_score = english_score;
    }//设置当前对象的English_score值,
    public void setMaths_score(double maths_score) {
        this.maths_score = maths_score;
    }//设置当前对象的Maths_score值,
    public double getTotalScore() {
        return computer_score + maths_score + english_score;
    }// 计算Computer_score, Maths_score 和English_score 三门课的总成绩。
    public double getAveScore() {
        return getTotalScore() / 3;
    }// 计算Computer_score, Maths_score 和English_score 三门课的平均成绩。
}
class Undergraduate extends Student2 {
    private String classID;
    public Undergraduate(String id, String name, char sex, int age, String classID) {
        super(id, name, sex, age);
        this.classID = classID;
    }
    public String getClassID() {
        return classID;
    }
    public void setClassID(String classID) {
        this.classID = classID;
    }
}
