import java.util.Collection;
import java.util.Iterator;
import java.util.TreeMap;

public class StudentTest {
    public static void main(String args[ ]) {
        TreeMap<StudentCompare,Student> treemap= new TreeMap<StudentCompare,Student>();
        String name[]={"罗乐琦","张斌","曾祥杰","罗雨石","姚明宇"};
        String sex[]={"boy","boy","boy","boy","boy"};
        int ID[]={19,20,21,22,23};
        int age[]={20,20,19,20,19};
        double math[]={80,86,100,70,89};
        double computer[]={99,97,100,86,95};
        double english[]={80,87,90,74,98};
        Student student[]=new Student[5];
        for(int i=0;i<student.length;i++) {
            student[i]=new Student(name[i],ID[i],sex[i],age[i],math[i],computer[i],english[i]);
        }
        StudentCompare key[]=new StudentCompare[5] ;
        for(int i=0;i<key.length;i++) {
            key[i]=new StudentCompare(student[i].getId());
        }
        for(int i=0;i<student.length;i++) {
            treemap.put(key[i],student[i]);  //put方法添加结点
        }
        int number=treemap.size();
        System.out.println("按学号升序排序：");
        Collection<Student> collection=treemap.values();//遍历
        Iterator<Student> iter=collection.iterator();//迭代
        while(iter.hasNext()) {
            Student stu=iter.next();
            System.out.println(stu.getName()+" ID："+stu.getId()+"性别："+stu.getSex()+" 年龄："+stu.getAge()+" 英语成绩："+stu.getEnglish_score()+" 计算机成绩:"+stu.getComputer_score()+" 数学成绩:"+stu.getMaths_score()+" 总成绩："+stu.getTotalScore()+" 平均成绩："+stu.getAveScore());
        }
        treemap.clear();
        for(int i=0;i<key.length;i++) {
            key[i]=new StudentCompare(student[i].getTotalScore());
        }
        for(int i=0;i<student.length;i++) {
            treemap.put(key[i],student[i]);
        }
        number=treemap.size();
        System.out.println("按总成绩升序排序：");
        collection=treemap.values();
        iter=collection.iterator();
        while(iter.hasNext()) {
            Student stu=(Student)iter.next();
            System.out.println(stu.getName()+" ID："+stu.getId()+"性别："+stu.getSex()+" 年龄："+stu.getAge()+" 英语成绩："+stu.getEnglish_score()+" 计算机成绩:"+stu.getComputer_score()+" 数学成绩:"+stu.getMaths_score()+" 总成绩："+stu.getTotalScore()+" 平均成绩："+stu.getAveScore());
        }
    }
}
