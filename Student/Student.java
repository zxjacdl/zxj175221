class StudentCompare implements Comparable {
    double d=0;
    StudentCompare (double d) {
        this.d=d;
    }
    public int compareTo(Object b) {
        StudentCompare st=(StudentCompare)b;
        if((this.d-st.d)==0)
        {
            return -1;
        }
        else
        {
            return (int)((this.d-st.d)*1000);
        }
    }
}
class Student  {
    private String name;//表示姓名
    private String sex;//表示性别
    private int id;//表示学号
    private int age;//表示年龄
    private double computer_score;//表示计算机课程的成绩
    private double english_score;//表示英语课的成绩
    private double maths_score;//表示数学课的成绩
    private double total_score = 0;// 表示总成绩
    private double ave_score=0;//表示平均成绩
    public Student(String name, int id,String sex,int age,double maths_score,double english_score,double computer_score) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.maths_score=maths_score;
        this.english_score=english_score;
        this.computer_score=computer_score;
    }
    public int getId() {
        return id;
    }//获得当前对象的学号
    public double  getComputer_score(){
        return computer_score;
    }//获得当前对象的计算机课程成绩
    public double getMaths_score() {
        return maths_score;
    }//获得当前对象的数学课程成绩
    public double getEnglish_score() {
        return english_score;
    }//获得当前对象的英语课程成绩
    public int getAge() {
        return age;
    }//获得当前对象的年龄
    public String getName() {
        return name;
    }//获得当前对象的名字
    public String getSex() {
        return sex;
    }//获得当前对象的性别
    public double getTotalScore() {
        return total_score = computer_score+maths_score + english_score;
    }// 计算Computer_score, Maths_score 和English_score 三门课的总成绩。
    public double getAveScore(){
        return getTotalScore()/3;
    }// 计算Computer_score, Maths_score 和English_score 三门课的平均成绩。
}


