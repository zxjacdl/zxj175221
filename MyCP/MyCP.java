import java.io.*;
import java.util.Scanner;
public class MyCP {
    public static void main(String[] args) throws IOException {
        //输入十进制文本
        Scanner scan=new Scanner(System.in);
        String str=scan.nextLine();
        String[]ch=str.split(" ");
        //调用tx方法把文本文件转化为二进制文件
        if(ch[2].equals("-tx")){
            FileReader reader = new FileReader("/home/zxj/twoweek/MyCP/testorigin.txt");
            FileWriter writer = new FileWriter("/home/zxj/twoweek/MyCP/testchange.bin");
            tx(reader, writer);
        }
        //用xt方法把二进制文件把转化为文本文件
        if(ch[2].equals("-xt")){
            FileReader reader = new FileReader("/home/zxj/twoweek/MyCP/testchange.bin");
            FileWriter writer = new FileWriter("/home/zxj/twoweek/MyCP/testchange2.txt");
            xt(reader, writer);
        }
    }
    public static void tx(FileReader reader, FileWriter writer) {
        try (FileReader In = reader; FileWriter Out = writer) {
            char[] number = new char[1];
            while((In.read(number))!=-1) {//FileReader的read()方法获取对应的十进制数字
                int number1=(int)number[0];
                String result="";
                String totwo=Integer.toBinaryString(number1);//调用Integer.toBinaryString将十进制转为二进制
                for(int i=0;i<8;i++){//返回一个二进制的无符号整数
                    if(i<(8-totwo.length())) {
                        result = result+'0';
                    }
                }
                result=result+totwo;
                Out.write(result, 0, 8);//FileWriter的write()方法保存得到的二进制
                Out.write(" ");
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void xt(FileReader reader, FileWriter writer) {
        try (FileReader In = reader; FileWriter Out = writer) {
            char[] number = new char[8];
            char[] ch = new char[1];
            while((In.read(number))!=-1) {
                String result="";
                In.read();//FileReader的read()方法获取对应的8位二进制数字
                for(int i=0;i<8;i++){
                    result=result+number[i];
                }
                int ten=Integer.valueOf(result,2);//Integer.valueOf()方法将二进制转为十进制
                ch[0]=(char)ten;
                Out.write(ch, 0, 1);//FileWriter的write()方法保存得到的十进制
            }
        }catch (IOException e) {
            e.printStackTrace();
        }

    }

}

