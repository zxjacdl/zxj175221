import junit.framework.*;

import org.junit.Test;

import java.util.Arrays;

public class TestArrays extends TestCase {

    int[] c = new int[]{2,3,1,9,5,0,8,6,7};

    int[] d = new int[] {9, 8, 1, 7, 6, 2};

    @Test

    public void testBinarySearch() throws Exception{
        assertEquals(0,Arrays.binarySearch(c,2));
        assertEquals(4,Arrays.binarySearch(d,6));
        assertEquals(1,Arrays.binarySearch(c,3));
        assertEquals(-4,Arrays.binarySearch(d,5));
    }

    @Test

    public void testSort() throws Exception{
        Arrays.sort(c);
        Arrays.sort(d);
        assertEquals(0,c[0]);//边界情况
        assertEquals(7,c[6]);//正常情况
        assertEquals(9,d[5]);//边界情况
        assertEquals(7,d[3]);//正常情况
    }




}
