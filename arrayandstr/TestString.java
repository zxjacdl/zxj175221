import junit.framework.*;

import org.junit.Test;

import java.util.Arrays;

public class TestString extends TestCase {

    String a = new String("zengxiangjie.");
    String b = new String("zeng-xiang-jie");
    String split = "Java is my favourite class";

    @Test
    public void testcharAt()  throws Exception{
        assertEquals('z',a.charAt(0));//边界情况
        assertEquals('g',a.charAt(8));//正常情况多选一
        assertEquals('.',a.charAt(12));//正常情况
    }

    @Test
    public void testSplit() {
        assertEquals("Java",split.split(" ")[0]);//返回字符数组,边界情况
        assertEquals("favourite",split.split(" ")[3]);//返回字符数组,正常情况
        assertEquals("zeng",b.split("-")[0]);//返回字符数组,边界情况
        assertEquals("xiang",b.split("-")[1]);//返回字符数组,正常情况

    }
}


