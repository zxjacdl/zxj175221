public class Bookshelf {
    public static void main(String[] args){

        Book book1 = new Book("Java2实用教程","耿祥义","清华大学出版社","2017.05");
        Book book2 = new Book("计算机网络","谢希仁","电子工业出版社","2017.01");
        Book book3 = new Book("算法与数据结构","张乃孝","高等教育出版社","2011.06");
        Book book4 = new Book("汇编语言程序设计","钱晓捷","电子工业出版社","2012.06");
        Book book5 = new Book("算法与数据结构","张乃孝","高等教育出版社","2011.06");

        System.out.println("bookname："+book1.getBookname());
        System.out.println("writer："+book1.getWriter());
        System.out.println("publishing："+book1.getPublishing());
        System.out.println("publishingdate："+book1.getPublishingdate()+"\n");

        System.out.println("bookname："+book2.getBookname());
        System.out.println("writer："+book2.getWriter());
        System.out.println("publishing："+book2.getPublishing());
        System.out.println("publishingdate："+book2.getPublishingdate()+"\n");

        System.out.println("bookname："+book3.getBookname());
        System.out.println("writer："+book3.getWriter());
        System.out.println("publishing："+book3.getPublishing());
        System.out.println("publishingdate："+book3.getPublishingdate()+"\n");

        System.out.println("bookname："+book4.getBookname());
        System.out.println("writer："+book4.getWriter());
        System.out.println("publishing："+book4.getPublishing());
        System.out.println("publishingdate："+book4.getPublishingdate()+"\n");

        System.out.println("bookname："+book5.getBookname());
        System.out.println("writer："+book5.getWriter());
        System.out.println("publishing："+book5.getPublishing());
        System.out.println("publishingdate："+book5.getPublishingdate()+"\n");

        System.out.println(book4.equals(book2));
        System.out.println(book5.equals(book3));
        System.out.println(book1.equals(book2));


    }
}


