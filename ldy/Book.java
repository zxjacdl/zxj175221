public class Book {
    private String bookname;
    private String writer;
    private String publishing;
    private String publishingdate;

    //定义getter
    public String getBookname() {//返回书名
        return bookname;
    }
    public String getWriter() {//返回作者
        return writer;
    }
    public String getPublishing() {//返回出版社
        return publishing;
    }
    public String getPublishingdate() {//返回出版日期
        return publishingdate;
    }
    
    //定义setter
    public void setBookname(String bookname) {
        this.bookname = bookname;
    }
    public void setWriter(String writer) {
        this.writer = writer;
    }
    public void setPublishing(String publishing) {
        this.publishing = publishing;
    }
    public void setpublishingdate(String publishingdate) {
        this.publishingdate = publishingdate;
    }

    //注入string属性
    public Book(String bookname, String writer, String publishing, String publishingdate) {
        this.bookname = bookname;
        this.writer = writer;
        this.publishing = publishing;
        this.publishingdate = publishingdate;
    }

    //覆盖toString方法
    public void toString(Book book) {
        System.out.println("bookname："+book.getBookname());
        System.out.println("writer："+book.getWriter());
        System.out.println("publishing："+book.getPublishing());
        System.out.println("publishingdate："+book.getPublishingdate());
    }

    //覆盖equals方法
    public boolean equals(Object o) {//比较两本书是否一致,一致返回true,反之返回false
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Book book = (Book) o;
        if (bookname != null ? !bookname.equals(book.bookname) : book.bookname != null)
            return false;
        if (writer != null ? !writer.equals(book.writer) : book.writer != null)
            return false;
        if (publishing != null ? !publishing.equals(book.publishing) : book.publishing != null)
            return false;
        return publishingdate != null ? publishingdate.equals(book.publishingdate) : book.publishingdate == null;
    }
}


